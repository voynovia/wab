Pod::Spec.new do |s|
  s.name         = "WaB"
  s.version      = "0.0.1"
  s.summary      = "Weight and Balance"
  s.description  = <<-DESC
                   Weight and Balance module for aircrafts
                   DESC
  s.homepage     = "https://www.baionline.ru/"
  s.license      = "MIT"
  s.author             = { "Igor Voynov" => "igyo@me.com" }
  s.ios.deployment_target = "9.3"
  s.source       = { :git => "https://gitlab.com/voynovia/wab.git", :tag => "#{s.version}" }
  s.source_files  = "WaB/**/*.swift"
  
  #s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"
  s.pod_target_xcconfig   = { 'SWIFT_VERSION' => '4' }
end
