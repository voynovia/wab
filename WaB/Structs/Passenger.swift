//
//  Passenger.swift
//  WaB
//
//  Created by Igor Voynov on 25.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public class Passenger {
  public let name: String
  public let weight: Double
  public var count: Int
  
  public init(name: String, weight: Double, count: Int) {
    self.name = name
    self.weight = weight
    self.count = count
  }
}
