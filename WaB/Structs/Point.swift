//
//  GraphPoint.swift
//  WaB
//
//  Created by Igor Voynov on 14.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public struct Point {
  public let index: Double
  public let weight: Double
  public let mac: Double
  public let isGood: Bool
}
