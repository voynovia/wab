//
//  PantryCode.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftPantryCode {
  public let uuid: String
  public let name: String
  public let weight: Double
  public let index: Double
  
  public init(uuid: String, name: String, weight: Double, index: Double) {
    self.name = name
    self.weight = weight
    self.index = index
    self.uuid = uuid
  }
}
