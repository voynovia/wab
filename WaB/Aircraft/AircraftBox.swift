//
//  GraphLine.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftBox {
  public let forward: [AircraftLimit]
  public let aft: [AircraftLimit]
  public let description: String?
  public let type: String?
  
  public init(forward: [AircraftLimit], aft: [AircraftLimit], description: String?, type: String?) {
    self.forward = forward
    self.aft = aft
    self.description = description
    self.type = type
  }
}
