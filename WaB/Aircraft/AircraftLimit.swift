//
//  GraphPoint.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftLimit {
  public let weight: Double
  public let index: Double
  
  public init(weight: Double, index: Double) {
    self.weight = weight
    self.index = index
  }
}
