//
//  AircraftPassenger.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftPassenger {
  public let name: String
  public let weightSummer: Double
  public let weightWinter: Double
  
  public init(name: String, weightSummer: Double, weightWinter: Double) {
    self.name = name
    self.weightSummer = weightSummer
    self.weightWinter = weightWinter
  }
}
