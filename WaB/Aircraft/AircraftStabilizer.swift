//
//  StabilizerTrim.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftStabilizer {
  public let mac: Int
  public let weight: Double
  
  public init(mac: Int, weight: Double) {
    self.mac = mac
    self.weight = weight
  }
}
