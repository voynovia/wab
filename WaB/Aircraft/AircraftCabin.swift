//
//  AircraftCabin.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftCabin {
  public let name: String
  public let sta: Double
  public var countMax: Int
  
  public init(name: String, sta: Double, countMax: Int) {
    self.name = name
    self.sta = sta
    self.countMax = countMax
  }
}
