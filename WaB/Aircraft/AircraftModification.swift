//
//  Modification.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftModification {
  
  public let uuid: String
  public let name: String
  public let taxiWeight: Double
  public let takeOffWeight: Double
  public let landingWeight: Double
  public let zeroFuelWeight: Double
  public let emptyWeight: Double
  public let emptyIndex: Double
  public var crews: [AircraftCrewCode]
  public var pantries: [AircraftPantryCode]
  public var dods: [AircraftDod]
  
  public init(uuid: String, name: String, taxiWeight: Double, takeOffWeight: Double, landingWeight: Double,
              zeroFuelWeight: Double, emptyWeight: Double, emptyIndex: Double,
              crews: [AircraftCrewCode], pantries: [AircraftPantryCode], dods: [AircraftDod]) {
    self.uuid = uuid
    self.name = name
    self.taxiWeight = taxiWeight
    self.takeOffWeight = takeOffWeight
    self.landingWeight = landingWeight
    self.zeroFuelWeight = zeroFuelWeight
    self.emptyWeight = emptyWeight
    self.emptyIndex = emptyIndex
    self.crews = crews
    self.pantries = pantries
    self.dods = dods
  }
}
