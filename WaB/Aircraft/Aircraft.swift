//
//  Aircraft.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class Aircraft {
  
  public let uuid: String
  public let name: String
  public let mac: Double
  public let refSta: Double
  public let lemac: Double
  
  public let mtowBox: AircraftBox
  public let mlwBox: AircraftBox
  public let mzfwBox: AircraftBox
  public let dtowBox: AircraftBox?
  public let dlwBox: AircraftBox?
  public let dzfwBox: AircraftBox?
  
  public let stabilizerMin: AircraftStabilizer
  public let stabilizerMax: AircraftStabilizer
  
  public let passengers: [AircraftPassenger]
  public let modifications: [AircraftModification]
  public let cabins: [AircraftCabin]
  public let cargos: [AircraftCargo]
  public let fuelEffects: [AircraftFuelEffect]
//  public let fuelTaxi: Double
  
  public let allowSecondAttendant: Bool
  public let secondAttendantWeight: Double
  public let secondAttendantSeatPerWeight: Double
  
  public init(uuid: String, name: String, mac: Double, refSta: Double, lemac: Double,
              mtowBox: AircraftBox, mlwBox: AircraftBox, mzfwBox: AircraftBox,
              dtowBox: AircraftBox?, dlwBox: AircraftBox?, dzfwBox: AircraftBox?,
              stabilizerMin: AircraftStabilizer, stabilizerMax: AircraftStabilizer,
              passengers: [AircraftPassenger], modifications: [AircraftModification],
              cabins: [AircraftCabin], cargos: [AircraftCargo], fuelEffects: [AircraftFuelEffect],
              allowSecondAttendant: Bool, secondAttendantWeight: Double, secondAttendantSeatPerWeight: Double) {
    self.uuid = uuid
    self.name = name
    self.mac = mac
    self.refSta = refSta
    self.lemac = lemac
    self.mtowBox = mtowBox
    self.mlwBox = mlwBox
    self.mzfwBox = mzfwBox
    self.dtowBox = dtowBox
    self.dlwBox = dlwBox
    self.dzfwBox = dzfwBox
    self.stabilizerMin = stabilizerMin
    self.stabilizerMax = stabilizerMax
    self.passengers = passengers
    self.modifications = modifications
    self.cabins = cabins
    self.cargos = cargos
    self.fuelEffects = fuelEffects
    self.allowSecondAttendant = allowSecondAttendant
    self.secondAttendantWeight = secondAttendantWeight
    self.secondAttendantSeatPerWeight = secondAttendantSeatPerWeight
  }
}
