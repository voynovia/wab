//
//  CrewCode.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftCrewCode {
  public let uuid: String
  public let cockpit: Int
  public let cabin: Int
  
  public init(uuid: String, cockpit: Int, cabin: Int) {
    self.cockpit = cockpit
    self.cabin = cabin
    self.uuid = uuid
  }
}
