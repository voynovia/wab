//
//  AircraftCargo.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftCargo {
  public let name: String
  public let sta: Double
  public let grossWeight: Double
  
  public init(name: String, sta: Double, grossWeight: Double) {
    self.name = name
    self.sta = sta
    self.grossWeight = grossWeight
  }
}
