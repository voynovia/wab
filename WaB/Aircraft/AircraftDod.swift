//
//  Dod.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

open class AircraftDod {
  public let pantryUUID: String
  public let crewUUID: String
  public let dow: Double
  public let doi: Double
  
  public init(pantryUUID: String, crewUUID: String, dow: Double, doi: Double) {
    self.pantryUUID = pantryUUID
    self.crewUUID = crewUUID
    self.dow = dow
    self.doi = doi
  }
}
