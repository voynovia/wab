//
//  GraphController.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public typealias Result = (tPoint: Point, lPoint: Point, zPoint: Point, stabValue: Double)

internal var aircraft: Aircraft!

open class WaB {

  public lazy var graph: Graph = Graph()
  
  public init(for air: Aircraft) {
    aircraft = air
  }
  
  public func getFuelIndex(block: Double) -> Double {
    var diff: Double = Double(Int.max)
    var fuelEffect: AircraftFuelEffect!
    for effect in aircraft.fuelEffects {
      let diffEffect = abs(block - effect.weight)
      if diffEffect < diff {
        diff = diffEffect
        fuelEffect = effect
      } else {
        break
      }
    }
    
    let fuelEffectPrMac = Math.prMAC(for: aircraft, index: fuelEffect.index, weight: fuelEffect.weight)
    return Math.index(for: aircraft, prMac: fuelEffectPrMac, weight: block)
  }
  
  // swiftlint:disable:next function_parameter_count
  public func process(doi: Double, dow: Double,
                      passengersIndex: Double, passengersWeight: Double,
                      cargosIndex: Double, cargosWeight: Double,
                      fuelBlock: Double, fuelTrip: Double,
                      completion: (Result) -> Void) {
    let zfw = dow + cargosWeight + passengersWeight
    let zfi = doi + passengersIndex + cargosIndex
    let tow = zfw + fuelBlock // - aircraft.fuelTaxi
    let toi = zfi + getFuelIndex(block: fuelBlock)
    let lw = tow - fuelTrip
    let li = toi - getFuelIndex(block: fuelTrip)
    
    let tPoint = Point(index: toi,
                       weight: tow,
                       mac: Math.prMAC(for: aircraft, index: toi, weight: tow),
                       isGood: Math.isGood(box: aircraft.mtowBox, weight: tow, index: toi))
    
    let lPoint = Point(index: li,
                       weight: lw,
                       mac: Math.prMAC(for: aircraft, index: li, weight: lw),
                       isGood: Math.isGood(box: aircraft.mlwBox, weight: lw, index: li))
    
    let zPoint = Point(index: zfi,
                       weight: zfw,
                       mac: Math.prMAC(for: aircraft, index: zfi, weight: zfw),
                       isGood: Math.isGood(box: aircraft.mzfwBox, weight: zfw, index: zfi))
    
    let stabMac = Math.prMAC(for: aircraft, index: toi, weight: tow)
    let stabValue = Math.stab(for: aircraft, mac: stabMac)
        
    completion((tPoint: tPoint, lPoint: lPoint, zPoint: zPoint, stabValue: stabValue))
  }
}
