//
//  Math.swift
//  WaB
//
//  Created by Igor Voynov on 19.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import Foundation

public struct Math {
  
  private static let kConstant: Double = 0
  private static let cConstant: Double = 100
  
  public static func index(for aircraft: Aircraft, sta: Double) -> Double {
    return (sta - aircraft.refSta) / 100
  }

  public static func index(for aircraft: Aircraft, prMac: Double, weight: Double) -> Double {
    return (((prMac * aircraft.mac/100) - aircraft.refSta + aircraft.lemac) * weight / cConstant) + kConstant
  }

  public static func prMAC(for aircraft: Aircraft, index: Double, weight: Double) -> Double {
    return (((cConstant*(index-kConstant))/weight) + aircraft.refSta - aircraft.lemac) / (aircraft.mac / 100)
  }

  public static func stab(for aircraft: Aircraft, mac: Double) -> Double {
    let pr = (mac - Double(aircraft.stabilizerMin.mac)) * 100
      / Double(aircraft.stabilizerMax.mac - aircraft.stabilizerMin.mac)
    return Double(aircraft.stabilizerMax.weight - aircraft.stabilizerMin.weight) * pr
      / 100 + aircraft.stabilizerMin.weight
  }

  private static func isPointInside(points: [AircraftLimit], point: AircraftLimit) -> Bool {
    var inside = false
    var j = points.count - 1
    for i in 0..<points.count {
      let xi = points[i].index, yi = points[i].weight
      let xj = points[j].index, yj = points[j].weight
      if ((yi > point.weight) != (yj > point.weight))
        && (point.index < (xj - xi) * (point.weight - yi) / (yj - yi) + xi) {
        inside = !inside
      }
      j = i
    }
    return inside
  }

  public static func isGood(box: AircraftBox, weight: Double, index: Double) -> Bool {
    let forwardPoints = box.forward.sorted(by: {$0.weight < $1.weight})
    let aftPoints = box.aft.sorted(by: {$0.weight > $1.weight})
    let points = forwardPoints + aftPoints
    let point = AircraftLimit(weight: weight, index: index)
    return isPointInside(points: points, point: point)
  }
}

