//
//  GraphSettings.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

public struct Settings {
  static let fontScale: UIFont = UIFont.systemFont(ofSize: 12)
  static let fontWarnings: UIFont = UIFont.systemFont(ofSize: 12)
  static let fontStabilizer: UIFont = UIFont.systemFont(ofSize: 14)
  
  static let goodColor: UIColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
  static let badColor: UIColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
  
  static let dashSmall: CGFloat = 10
  static let dashMedium: CGFloat = 15
  static let dashBig: CGFloat = 20
}
