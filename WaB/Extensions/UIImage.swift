//
//  UIView+UIImage.swift
//  Pods-ATR42
//
//  Created by Igor Voynov on 22.12.2017.
//

import UIKit.UIImage
import UIKit.UIView

public extension UIImage {
  public convenience init(view: UIView) {
    UIGraphicsBeginImageContext(view.frame.size)
    view.layer.render(in:UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    self.init(cgImage: image!.cgImage!)
  }
  
  public func save(to url: URL) throws {
    enum SaveError: Error {
      case JPEGRepresentation
    }
    guard let data = UIImageJPEGRepresentation(self, 0.8) else {
      throw SaveError.JPEGRepresentation
    }
    try data.write(to: url)
  }
}
