//
//  UIColor.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
  static let backgroundGraph: UIColor = #colorLiteral(red: 0.8177424669, green: 0.8276742101, blue: 0.8404242396, alpha: 1)
}
