//
//  UILabel.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit.UILabel

public extension UILabel {
  func underLine() {
    if let textUnwrapped = self.text {
      let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
      let underlineAttributedString = NSAttributedString(string: textUnwrapped, attributes: underlineAttribute)
      self.attributedText = underlineAttributedString
    }
  }
}
