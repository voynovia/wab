//
//  WorkspaceView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class WorkspaceView: UIView {
  
  public var mtowBox: AircraftBox!
  public var mlwBox: AircraftBox!
  public var mzfwBox: AircraftBox!
  public var dtowBox: AircraftBox?
  public var dlwBox: AircraftBox?
  public var dzfwBox: AircraftBox?
  
  public var minX: Int = 0
  public var maxX: Int = 0
  public var minY: Int = 0
  public var maxY: Int = 0
  public var minTop: Int = 0
  public var maxTop: Int = 0
  
  private let lineWidth: CGFloat = 2.0
  
  private var xStep: CGFloat {
    return bounds.width / CGFloat((maxX+25) - (minX-25))
  }
  private var xZero: CGFloat {
    return CGFloat(abs(minX-25)) * xStep
  }
  private var yStep: CGFloat {
    return bounds.height / (CGFloat(maxY - minY) / 1000)
  }
  
  private var pointsView: UIView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .backgroundGraph
    layer.borderWidth = 1
    layer.borderColor = UIColor.black.cgColor
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func drawHorizontalLines() {
    for y in stride(from: yStep, to: bounds.maxY, by: yStep) {
      let line = UIBezierPath()
      line.move(to: CGPoint(x: 0, y: y))
      line.addLine(to: CGPoint(x: bounds.maxX, y: y))
      UIColor.lightGray.setStroke()
      line.stroke()
    }
  }
  
  private func drawVerticalLines() {
    let topStep = bounds.width / CGFloat(maxTop - minTop + 2)
    for percent in stride(from: minTop, through: maxTop, by: 1) {
      let startPoint = CGPoint(x: topStep * CGFloat(percent - minTop + 1), y: 0)
      let index = Math.index(for: aircraft, prMac: Double(percent), weight: Double(minY))
      let endPoint = CGPoint(x: xZero + (CGFloat(index) * xStep), y: bounds.maxY)
      let line = UIBezierPath()
      line.move(to: startPoint)
      line.addLine(to: endPoint)
      UIColor.lightGray.setStroke()
      line.stroke()
    }
  }
  
  private func getPoint(weight: Int, index: Double) -> CGPoint {
    var x = xZero + (CGFloat(index) * xStep)
    let y = CGFloat(maxY - weight) / 1000 * yStep
    x = x > bounds.minX && x < bounds.maxX ? x : bounds.minX
    return CGPoint(x: x, y: y)
  }
  
  private func drawLine(line: AircraftBox, fill: Bool) {
    let forwardPoints = line.forward.sorted(by: {$0.weight < $1.weight})
      .map { getPoint(weight: Int($0.weight), index: $0.index) }
    let aftPoints = line.aft.sorted(by: {$0.weight > $1.weight})
      .map { getPoint(weight: Int($0.weight), index: $0.index) }
    if let type = line.type,
      let first = forwardPoints.last,
      let second = aftPoints.first,
      let value = line.forward.last?.weight {
      addNameLine(firstPoint: first, secondPoint: second, type: type, value: String(value))
    }
    if aftPoints.count > 1,
      let description = line.description,
      let first = aftPoints.first,
      let second = aftPoints.last {
      addDescriptionLine(firstPoint: first, secondPoint: second, description: description)
    }
    let line = UIBezierPath()
    for (index, point) in (forwardPoints + aftPoints).enumerated() {
      if index == 0 {
        line.move(to: point)
      } else {
        line.addLine(to: point)
      }
    }
    if fill {
      line.close()
      UIColor.white.setFill()
      line.fill()
    }
    line.lineWidth = lineWidth
    UIColor.darkGray.setStroke()
    line.stroke()
  }
  
  private func addDescriptionLine(firstPoint: CGPoint, secondPoint: CGPoint, description: String) {
    let degree = atan((secondPoint.y - firstPoint.y) / (secondPoint.x - firstPoint.x))
    let label = UILabel()
    label.font = Settings.fontWarnings
    label.textColor = .black
    label.backgroundColor = .backgroundGraph
    label.textAlignment = .center
    label.text = description
    label.frame.size = label.intrinsicContentSize
    label.frame.origin = CGPoint(x: (firstPoint.x+secondPoint.x)/2-label.frame.size.width/2+label.frame.size.height/2,
                                 y: (firstPoint.y+secondPoint.y)/2)
    label.transform = CGAffineTransform(rotationAngle: degree)
    addSubview(label)
  }
  
  private func addNameLine(firstPoint: CGPoint, secondPoint: CGPoint, type: String, value: String) {
    guard let line = LineLabel(rawValue: type) else { return }
    let label = UILabel()
    label.text = line.name + "(\(value) kg)"
    label.textColor = line.textColor
    label.font = Settings.fontWarnings
    label.backgroundColor = line.backgroundColor
    let width = label.intrinsicContentSize.width
    let height = label.intrinsicContentSize.height
    let y = line.upPosition ? firstPoint.y - height - 3 : firstPoint.y + 3
    label.frame = CGRect(x: (secondPoint.x-firstPoint.x)/2 + firstPoint.x - width / 2, y: y,
                         width: width, height: height)
    addSubview(label)
    
    let badge = line.badge
    badge.frame.origin = CGPoint(x: secondPoint.x - badge.frame.width - 5, y: secondPoint.y - badge.frame.height / 2)
    addSubview(badge)
  }
  
  private func drawWarnings() {
    let lines: [LineLabel] = [.mlw, .mtow, .mzfw]
    var yDescription: CGFloat = 0
    for (index, line) in lines.enumerated() {
      let view = UIView()
      view.backgroundColor = .backgroundGraph
      let badgeLabel = line.badge
      badgeLabel.frame.origin.x = 20
      view.addSubview(badgeLabel)
      let descriptionLabel = UILabel()
      descriptionLabel.text = line.text
      descriptionLabel.font = Settings.fontWarnings
      descriptionLabel.frame = CGRect(x: badgeLabel.frame.maxX + 5,
                                      y: 0,
                                      width: descriptionLabel.intrinsicContentSize.width,
                                      height: badgeLabel.frame.height)
      view.addSubview(descriptionLabel)
      view.frame.origin.x = 0
      view.frame.origin.y = bounds.maxY - 5 - CGFloat(index+1) * badgeLabel.frame.height
      view.frame.size = CGSize(width: descriptionLabel.frame.maxX + 5, height: badgeLabel.frame.height)
      yDescription = view.frame.origin.y
      addSubview(view)
    }
    
    let label = getLabel(text: "  Operational Limits")
    yDescription -= label.intrinsicContentSize.height
    label.frame = CGRect(x: 0, y: yDescription,
                         width: label.intrinsicContentSize.width, height: label.intrinsicContentSize.height)
    addSubview(label)
        
    let warning = getLabel(text: """
    Do not operate
    in shaded area
    in commercial flight!
    """)
    warning.underLine()
    warning.numberOfLines = 0
    yDescription -= warning.intrinsicContentSize.height + 10
    warning.frame = CGRect(x: 5, y: yDescription,
                           width: warning.intrinsicContentSize.width, height: warning.intrinsicContentSize.height)
    addSubview(warning)
  }
  
  private func getLabel(text: String) -> UILabel {
    let label = UILabel()
    label.text = text
    label.font = Settings.fontWarnings
    label.textColor = .black
    label.backgroundColor = .backgroundGraph
    return label
  }
  
  public func clean() {
    pointsView?.removeFromSuperview()
  }
  
  public func drawPoints(tPoint: Point, lPoint: Point, zPoint: Point) {
    clean()
    var points = [PointsView.Point(coord: getPoint(weight: Int(zPoint.weight), index: zPoint.index),
                                   description: "ZFW" + " (\(zPoint.mac.rounded(toPlaces: 2))%MAC)",
      color: zPoint.isGood ? Settings.goodColor : Settings.badColor)]
    if tPoint.weight > zPoint.weight {
      points.append(PointsView.Point(coord: getPoint(weight: Int(tPoint.weight), index: tPoint.index),
                                     description: "TOW" + " (\(tPoint.mac.rounded(toPlaces: 2))%MAC)",
        color: tPoint.isGood ? Settings.goodColor : Settings.badColor))
    }
    if lPoint.weight < tPoint.weight {
      points.append(PointsView.Point(coord: getPoint(weight: Int(lPoint.weight), index: lPoint.index),
                                     description: "LW" + " (\(lPoint.mac.rounded(toPlaces: 2))%MAC)",
        color: lPoint.isGood ? Settings.goodColor : Settings.badColor))
    }
    pointsView = PointsView(points: points, frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
    pointsView.layer.zPosition = 999
    addSubview(pointsView)
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
        
    // Operational lines
    drawLine(line: mtowBox, fill: true)
    drawLine(line: mlwBox, fill: true)
    drawLine(line: mzfwBox, fill: true)
    
    drawHorizontalLines()
    drawVerticalLines()
    
    // Design lines
    if let dtowPoints = dtowBox {
      drawLine(line: dtowPoints, fill: false)
    }
    if let dlwPoints = dlwBox {
      drawLine(line: dlwPoints, fill: false)
    }

    drawWarnings()
  }
}
