//
//  IndexScaleView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class IndexScaleView: UIView {
  
  public var minValue: Int = Int.min
  public var maxValue: Int = Int.max
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private var descriptionLabel: UILabel {
    let label = UILabel()
    label.font = Settings.fontScale
    label.text = "INDEX"
    label.textColor = .darkGray
    return label
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    let step: CGFloat = rect.width / CGFloat((maxValue - minValue) / 10 + 5)
    
    let labelWidth = step * 2
    let labelHeight = step - Settings.dashBig
    
    let startLabel = descriptionLabel
    startLabel.frame = CGRect(x: 0, y: Settings.dashBig, width: labelWidth, height: labelHeight)
    addSubview(startLabel)
    
    let endLabel = descriptionLabel
    endLabel.frame = CGRect(x: rect.maxX - labelWidth, y: Settings.dashBig, width: labelWidth, height: labelHeight)
    endLabel.textAlignment = .right
    addSubview(endLabel)
    
    let range = ((minValue/10)-2)...(maxValue/10+2)
    let values = [Int](range).map { $0 * 10 }
    var index = 0
    
    for x in stride(from: 0, through: rect.width+1, by: step / 10) {
      let y0: CGFloat = floor(x.truncatingRemainder(dividingBy: step)) == 0 ? Settings.dashMedium : Settings.dashBig
      let y: CGFloat = floor(x.truncatingRemainder(dividingBy: step/2)) == 0 ? y0 : Settings.dashSmall
      
      if y == Settings.dashBig {
        let value = values[index]
        if value >= minValue && value <= maxValue {
          let label = UILabel(frame: CGRect(x: x-(step/2), y: Settings.dashBig, width: step, height: labelHeight))
          label.textAlignment = .center
          label.textColor = .darkGray
          label.font = Settings.fontScale
          label.text = String(abs(value))
          addSubview(label)
        }
        index += 1
      }
      
      let line = UIBezierPath()
      line.move(to: CGPoint(x: x, y: 0.0))
      line.addLine(to: CGPoint(x: x, y: y))
      UIColor.lightGray.setStroke()
      line.stroke()
    }
  }
  
}
