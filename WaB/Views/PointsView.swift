//
//  PointsView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class PointsView: UIView {
  
  struct Point {
    let coord: CGPoint
    let description: String
    let color: UIColor
  }
  
  let points: [Point]
  
  init(points: [Point], frame: CGRect) {
    self.points = points
    super.init(frame: frame)
    self.backgroundColor = .clear
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    let path = UIBezierPath()
    for (index, point) in points.enumerated() {
      if index == 0 {
        path.move(to: CGPoint(x: point.coord.x, y: point.coord.y))
      } else {
        path.addLine(to: CGPoint(x: point.coord.x, y: point.coord.y))
      }
      let crossView = CrossView(color: point.color,
                                frame: CGRect(x: point.coord.x-10, y: point.coord.y-10, width: 20, height: 20))
      addSubview(crossView)
      
      let label = UILabel()
      label.text = point.description
      label.font = UIFont.boldSystemFont(ofSize: 14)
      label.textColor = point.color
      label.frame.origin = CGPoint(x: crossView.frame.maxX + 5, y: crossView.frame.minY)
      label.frame.size = label.intrinsicContentSize
      addSubview(label)
    }
    path.lineWidth = 2
    #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1).setStroke()
    path.stroke()
  }
  
}
