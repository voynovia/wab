//
//  WeightScaleView.swift
//  WaB
//
//  Created by Igor Voynov on 12.12.2017.
//  Copyright © 2017 Igor Voynov. All rights reserved.
//

import UIKit

class WeightScaleView: UIView {
  
  public var minValue: Int = Int.min
  public var maxValue: Int = Int.max
  
  private let descriptionLabel: UILabel = {
    let label = UILabel()
    label.textColor = .darkGray
    label.font = Settings.fontScale
    label.text = "WEIGHT kg x 1000"
    label.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    
    let desc = descriptionLabel
    let height = desc.intrinsicContentSize.width
    desc.frame = CGRect(x: 0,
                        y: bounds.midY - height / 2,
                        width: desc.intrinsicContentSize.height,
                        height: height)
    addSubview(desc)
    
    let step = rect.height / CGFloat(maxValue - minValue)
    
    let values: [Int] = Array(minValue...maxValue).reversed()
    var index = 0
    
    let labelWidth = step - Settings.dashBig
    
    for y in stride(from: 0, through: rect.maxY, by: step / 10) {
      let x0: CGFloat = floor(y.truncatingRemainder(dividingBy: step)) == 0 ? Settings.dashBig : Settings.dashMedium
      let x: CGFloat = floor(y.truncatingRemainder(dividingBy: step/2)) == 0 ? x0 : Settings.dashSmall
      
      if x == Settings.dashBig {
        let value = values[index]
        if value >= minValue && value <= maxValue {
          let label = UILabel(frame: CGRect(x: rect.maxX - labelWidth - Settings.dashBig - 5,
                                            y: y-(step/2),
                                            width: labelWidth,
                                            height: step))
          label.textAlignment = .right
          label.textColor = .darkGray
          label.font = Settings.fontScale
          label.text = String(value)
          addSubview(label)
        }
        index += 1
      }
      
      let line = UIBezierPath()
      line.move(to: CGPoint(x: rect.maxX, y: y))
      line.addLine(to: CGPoint(x: rect.maxX - x, y: y))
      UIColor.lightGray.setStroke()
      line.stroke()
    }
    
  }
}
